#!/usr/bin/awk -f
BEGIN {
    print "Name\tAge\tC\tJava\tPHP\tAvg\tGraded";
    bad=0;
    average=0;
    good=0;
}
{
    score_avg=($3+$4+$5)/3;
    grade="Bad";
    if (score_avg >= 8) {
	grade="Good";
	good++;
    } else if (score_avg >= 7) {
	grade="Average";
	average++;
    } else {
	bad++;
    }
    print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"score_avg"\t"grade;
}
END {
    print "----------------------------------------------------------";
    printf "Bad: %s\n", bad;
    printf "Average: %s\n", average;
    printf "Good: %s\n", good;
}
